﻿[void][reflection.assembly]::LoadFile("C:\temp\microsoft.web.webview2.1.0.781-prerelease\lib\net45\Microsoft.Web.WebView2.WinForms.dll")
[void][reflection.assembly]::LoadFile("C:\temp\microsoft.web.webview2.1.0.781-prerelease\lib\net45\Microsoft.Web.WebView2.Core.dll")
[void][reflection.assembly]::Load('System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a')
[void][reflection.assembly]::Load('System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089')
$form = [System.Windows.Forms.Form]::New()
$webview = [Microsoft.Web.WebView2.WinForms.WebView2]::New()
$webview.CreationProperties = [Microsoft.Web.WebView2.WinForms.CoreWebView2CreationProperties]::New()
$webview.CreationProperties.UserDataFolder = "C:\temp"

$InitialFormWindowState = New-Object 'System.Windows.Forms.FormWindowState'
$form1_Load={
    #TODO: Initialize Form Controls here
    $webview.Source = ([uri]::new($textbox1.Text))
    $webview.Visible = $true
}

$buttonGo_Click={
    #TODO: Place custom script here
    $webview.Source = [System.Uri] $textbox1.Text;
}

$Form_StateCorrection_Load=
{
    #Correct the initial state of the form to prevent the .Net maximized form issue
    $form1.WindowState = $InitialFormWindowState
}

$Form_Cleanup_FormClosed=
{
    #Remove all event handlers from the controls
    try
    {
        $buttonGo.remove_Click($buttonGo_Click)
        $webview.remove_SourceChanged($webview_SourceChanged)
        $form1.remove_Load($form1_Load)
        $form1.remove_Load($Form_StateCorrection_Load)
        $form1.remove_FormClosed($Form_Cleanup_FormClosed)
    }
    catch { Out-Null <# Prevent PSScriptAnalyzer warning #> }
}

$form.SuspendLayout()
$form1.Controls.Add($buttonRefresh)
$form1.Controls.Add($buttonGo)
$form1.Controls.Add($textbox1)
$form1.Controls.Add($webview)
$form1.AutoScaleDimensions = New-Object System.Drawing.SizeF(6, 13)
$form1.AutoScaleMode = 'Font'
$form1.ClientSize = New-Object System.Drawing.Size(619, 413)
$form1.Name = 'form1'
$form1.Text = 'Form'
$form1.add_Load($form1_Load)